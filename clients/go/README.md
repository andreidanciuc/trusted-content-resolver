Keel here all useful information for Golang Client

To install golang interpreter see [2], base metal deployment on Mac/Ubuntu.

To set up the project [1] see usage of ``go mod download`` command, mostly targeting people new for Golang.


References::

[1] [Golang Environment – GOPATH vs go.mod](https://www.freecodecamp.org/news/golang-environment-gopath-vs-go-mod/)
[2] [ansible.playbook.yml](./ansible.playbook.yml)
