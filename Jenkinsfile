pipeline {
    agent any
    environment {
        SOURCE_PATHS = "src/trusted_content_resolver_client"
        VENV_PATH_DEV = ".venv"
        DOCKER_IMAGE_PYTHON = "python:3.11"
        DOCKER_IMAGE_JAVA = "maven:3.9.5-eclipse-temurin-21"
        PYLINTHOME = "${WORKSPACE}/.cache/pylint"
        PIP_CACHE_DIR = "${WORKSPACE}/.cache/pip"
        CACHE_DIR_M2 = "${WORKSPACE}/.cache/m2"
        MAVEN_OPTS="-Dmaven.repo.local='${CACHE_DIR_M2}' -Dmaven.test.skip=true" // FIXME test fail on maven:3.9.5-eclipse-temurin-21, fix this
        PORT_TCR = "16005"
        PORT_UNI_RESOLVER_WEB = "16100"
        TRAIN_BDD_PIPELINE="TRAIN-BDD-pipeline"
    }
    // agent none
    triggers {
        pollSCM "* * * * *"
    }
    stages {
//        stage("Clone") {
//            steps {
//                git url: "https://gitlab.eclipse.org/eclipse/xfsc/train/trusted-content-resolver.git", branch: "jenkins-2"
//           }
//        }

        stage("Prepare cache folders") {
            steps {
                // language=sh
                sh """#!/bin/bash
                set -x
                mkdir -p "${PYLINTHOME}/"
                mkdir -p "${PIP_CACHE_DIR}/"
                mkdir -p "${CACHE_DIR_M2}/"
                # env
                """
            }
        }

        stage("Spawn Language Pipeline") {
            parallel {
                stage("Java") {
                    stages {
                        stage("Assembly JARs") {
                            agent {
                                docker {
                                    image "${DOCKER_IMAGE_JAVA}"
                                    reuseNode true
                                }
                            }
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                mvn clean install
                                """
                            }
                        }

                        stage("Build docker image train/tcr-backend") {
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                cd service
                                docker build -t train/tcr-backend .
                                """
                            }
                        }

                        stage("Build docker image train/tcr-client") {
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                cd clients/java
                                docker build -t train/tcr-client .
                                """
                            }
                        }

                        stage("Stop/Remove universal did resolver if up") {
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                cd docker/
                                docker compose --env-file unires.env -f uni-resolver-web.yml stop
                                """
                            }
                        }
                        stage("Start universal did resolver") {
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                cd docker/
                                docker compose --env-file unires.env -f uni-resolver-web.yml up -d
                                """
                            }
                        }

                        stage("Stop/Remove TCR backend if up") {
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                docker rm --force tcr-backend || true
                                """
                            }
                        }
                        stage("Start TCR backend") {
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                set -x

                                docker run --network=host -d \
                                  --name=tcr-backend \
                                  -e SERVER_PORT="${PORT_TCR}" \
                                  -e TCR_DID_BASE_URI="http://localhost:${PORT_UNI_RESOLVER_WEB}/1.0" \
                                  train/tcr-backend
                                """
                            }
                        }

                    }
                }

                stage("Python") {
                    stages {
                        stage("Python - Build") {
                            agent {
                                docker {
                                    image "${DOCKER_IMAGE_PYTHON}"
                                    reuseNode true
                                }
                            }
                            steps {
                                // language=sh
                                sh """#!/bin/bash
                                
                                cd ./clients/py/
                                
                                python -m venv "${VENV_PATH_DEV}"
                                source "${VENV_PATH_DEV}/bin/activate"

                                pip install --upgrade pip wheel

                                # important (!) keep `--editable`, code coverage will not work without it.
                                pip install --editable ".[dev]"
                                """
                            }
                        }
                        stage("Python - Test") {
                            stages {
                                stage("isort") {
                                    agent {
                                        docker {
                                            image "${DOCKER_IMAGE_PYTHON}"
                                            reuseNode true
                                        }
                                    }
                                    steps {
                                        // language=sh
                                        sh """#!/bin/bash
                                        cd ./clients/py/
                                        source "${VENV_PATH_DEV}/bin/activate"
                                        isort --check "${SOURCE_PATHS}" tests
                                        """
                                    }
                                }
                                stage("pylint") {
                                    agent {
                                        docker {
                                            image "${DOCKER_IMAGE_PYTHON}"
                                            reuseNode true
                                        }
                                    }
                                    steps {
                                        // language=sh
                                        sh """#!/bin/bash
                                        cd ./clients/py/
                                        source "${VENV_PATH_DEV}/bin/activate"
                                        pylint "${SOURCE_PATHS}" tests
                                        """
                                    }
                                }
                                stage("coverage") {
                                    agent {
                                        docker {
                                            image "${DOCKER_IMAGE_PYTHON}"
                                            reuseNode true
                                        }
                                    }
                                    steps {
                                        // language=sh
                                        sh """#!/bin/bash
                                        cd ./clients/py/
                                        source "${VENV_PATH_DEV}/bin/activate"
                                        coverage run -m pytest -m "not integration"
                                        coverage report
                                        """
                                    }
                                }
                                stage("mypy") {
                                    agent {
                                        docker {
                                            image "${DOCKER_IMAGE_PYTHON}"
                                            reuseNode true
                                        }
                                    }
                                    steps {
                                        // language=sh
                                        sh """#!/bin/bash
                                        cd ./clients/py/
                                        source "${VENV_PATH_DEV}/bin/activate"
                                        mypy "${SOURCE_PATHS}"
                                        """
                                    }
                                }
                                stage("licensecheck") {
                                    agent {
                                        docker {
                                            image "${DOCKER_IMAGE_PYTHON}"
                                            reuseNode true
                                        }
                                    }
                                    steps {
                                        // language=sh
                                        sh """#!/bin/bash
                                        cd ./clients/py/
                                        source "${VENV_PATH_DEV}/bin/activate"
                                        set -x
                                        mkdir -p .tmp/
                                        cd .tmp/
                                        pip freeze > requirements.txt
                                        licensecheck -u requirements > ../THIRD-PARTY.txt
                                        # TODO: open a tread in MR to resolve drifted license or version in case needed                                        
                                        """
                                    }
                                    post {
                                        always {
                                            archiveArtifacts artifacts: "clients/py/THIRD-PARTY.txt" //, onlyIfSuccessful: true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                stage("Golang") {
                    steps {
                        // language=sh
                        sh """#!/bin/bash
                        """
                    }
                }

                stage("JavaScript") {
                    steps {
                        // language=sh
                        sh """#!/bin/bash
                        """
                    }
                }

            }

        }

        stage("Wait for UNI_RESOLVER_WEB to be ready") {
            steps {
                timeout(time: 3, unit: "MINUTES") {
                    // language=sh
                    sh """#!/bin/bash
                        set -x
                        set +e
                        while ! curl http://localhost:${PORT_UNI_RESOLVER_WEB}/actuator/health
                        do
                          echo 
                          echo "[INFO] still is not up, retry in 10 seconds.."
                          sleep 10
                        done
                        """
                }
            }
        }
        stage ("Invoke BDD pipeline") {
            steps {
                build job: "${TRAIN_BDD_PIPELINE}", parameters: [
                        string(name: "some-param1", value: "some-value1")
                ]
            }

        }
    }

}