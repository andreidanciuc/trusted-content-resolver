package eu.xfsc.train.tcr.server.model;

import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class TrustListWithHash {
	
	private String content;
	private TrustStatusListType trustList;
	private String hash;

}
