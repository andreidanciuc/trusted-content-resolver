package eu.xfsc.train.tcr.server.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.List;

import org.jitsi.dnssec.validator.ValidatingResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.SimpleResolver;

import com.apicatalog.jsonld.document.Document;
import com.apicatalog.jsonld.loader.DocumentLoader;
import com.danubetech.verifiablecredentials.jsonld.VerifiableCredentialContexts;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import org.xbill.DNS.Resolver;

import eu.xfsc.train.tcr.server.exception.DidException;
import eu.xfsc.train.tcr.server.exception.DnsException;
import eu.xfsc.train.tcr.server.model.TrustListWithHash;
import eu.xfsc.train.tcr.server.service.WrapperResolver;
import foundation.identity.did.DIDDocument;
import foundation.identity.jsonld.ConfigurableDocumentLoader;
import lombok.extern.slf4j.Slf4j;
import uniresolver.UniResolver;
import uniresolver.client.ClientUniResolver;

@Slf4j
@Configuration
public class ResolverConfig {

    private final static String DNSROOT = ". IN DNSKEY 257 3 8 AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3+/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kvArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+eoZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfdRUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwNR1AkUTV74bU=";
	
	@Bean
	public UniResolver uniResolver(@Value("${tcr.did.base-uri}") String baseUri,
			@Value("${tcr.did.timeout}") int timeout) {
		log.info("uniResolver.enter; configured base URI: {}, timeout: {}", baseUri, timeout);
		UniResolver resolver;
		URI uri;
		try {
			uri = new URI(baseUri);
		} catch (URISyntaxException ex) {
			log.error("uniResolver.error", ex);
			throw new DidException(ex);
		}
		resolver = ClientUniResolver.create(uri);
		log.info("uniResolver.exit; returning resolver: {}", resolver);
		return resolver;
	}

	@Bean
	public Resolver resolver(@Value("${tcr.dns.hosts}") List<String> dnsHosts,
			@Value("${tcr.dns.timeout}") int timeout,
		    @Value("${tcr.dns.dnssec.rootPath}") String dnssecRootPath, 
		    @Value("${tcr.dns.dnssec.enabled}") boolean dnssecEnabled) {
		log.info("resolver.enter; configured DNS hosts: {}, timeout: {}, dnssecRootPath: {}, dnssecEnaabled: {}", dnsHosts, timeout, dnssecRootPath, dnssecEnabled);
		Resolver resolver;
		try {
			if (dnsHosts.isEmpty()) {
				resolver = new SimpleResolver();
			} else {
				SimpleResolver[] resolvers = new SimpleResolver[dnsHosts.size()];
				int idx = 0;
				for (String dnsHost : dnsHosts) {
					SimpleResolver r = new SimpleResolver(dnsHost);
					if (timeout > 0) {
						r.setTimeout(Duration.ofMillis(timeout));
					}
					resolvers[idx++] = r;
				}
				resolver = new ExtendedResolver(resolvers);
			}
			if (timeout > 0) {
				resolver.setTimeout(Duration.ofMillis(timeout));
			}
			
			if (dnssecEnabled) {
				InputStream rootInputStream;
				File f = new File(dnssecRootPath);
				if (f.exists() && !f.isDirectory()) {
			        rootInputStream = new FileInputStream(dnssecRootPath);
			    } else {
			        rootInputStream = getClass().getClassLoader().getResourceAsStream(dnssecRootPath);
			        if (rootInputStream == null) {
			            log.info("resolve; DNSSEC Root-key not found, using hardcoded backup.");
			            rootInputStream = new ByteArrayInputStream(DNSROOT.getBytes());
			        }
			    }

		        WrapperResolver validatingResolver = new WrapperResolver(resolver);
		        validatingResolver.loadTrustAnchors(rootInputStream);
		        resolver = validatingResolver;
			}
		} catch (IOException ex) {
			log.error("resolver.error", ex);
			throw new DnsException(ex);
		}
		log.info("resolver.exit; returning resolver: {}", resolver);
		return resolver;
	}

	@Bean
	public DocumentLoader documentLoader(Cache<URI, Document> docLoaderCache) {
		log.info("documentLoader.enter; cache: {}", docLoaderCache);
		ConfigurableDocumentLoader loader = (ConfigurableDocumentLoader) VerifiableCredentialContexts.DOCUMENT_LOADER;
		// TODO: get settings from app props
		loader.setEnableFile(true);
		loader.setEnableHttp(true);
		loader.setEnableHttps(true);
		loader.setEnableLocalCache(false);
		loader.setRemoteCache(docLoaderCache);
		log.info("documentLoader.exit; returning: {}", loader);
		return loader;
	}
	
	@Bean
	public Cache<String, DIDDocument> didDocumentCache(@Value("${tcr.did.cache.size}") int cacheSize,
			@Value("${tcr.did.cache.timeout}") Duration timeout) {
		log.info("didDocumentCache.enter; cache size: {}, ttl: {}", cacheSize, timeout);
        Caffeine<?, ?> cache = Caffeine.newBuilder().expireAfterAccess(timeout); 
        if (cacheSize > 0) {
            cache = cache.maximumSize(cacheSize);
        } 
		log.info("didDocumentCache.exit; returning: {}", cache);
        return (Cache<String, DIDDocument>) cache.build();
	}
	
	@Bean
	public Cache<URI, Document> docLoaderCache(@Value("${tcr.did.cache.size}") int cacheSize,
			@Value("${tcr.did.cache.timeout}") Duration timeout) {
		log.info("docLoaderCache.enter; cache size: {}, ttl: {}", cacheSize, timeout);
        Caffeine<?, ?> cache = Caffeine.newBuilder().expireAfterAccess(timeout); 
        if (cacheSize > 0) {
            cache = cache.maximumSize(cacheSize);
        } 
        //if (synchronizer != null) {
        //    cache = cache.removalListener(new DataListener<>(synchronizer));
        //}
		log.info("docLoaderCache.exit; returning: {}", cache);
        return (Cache<URI, Document>) cache.build();
	}
	
	@Bean
	public Cache<String, TrustListWithHash> trustListCache(@Value("${tcr.tl.cache.size}") int cacheSize,
			@Value("${tcr.tl.cache.timeout}") Duration timeout) {
		log.info("trustListCache.enter; cache size: {}, ttl: {}", cacheSize, timeout);
        Caffeine<?, ?> cache = Caffeine.newBuilder().expireAfterAccess(timeout); 
        if (cacheSize > 0) {
            cache = cache.maximumSize(cacheSize);
        } 
		log.info("trustListCache.exit; returning: {}", cache);
        return (Cache<String, TrustListWithHash>) cache.build();
	}

}

