package eu.xfsc.train.tcr.server.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.OptionalInt;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Cache;

import eu.europa.esig.dss.enumerations.MimeType;
import eu.europa.esig.dss.enumerations.MimeTypeEnum;
import eu.europa.esig.trustedlist.TrustedListFacade;
import eu.europa.esig.trustedlist.jaxb.tsl.TrustStatusListType;
import eu.xfsc.train.tcr.server.model.TrustListWithHash;
import io.ipfs.multihash.Multihash;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TLResolver {
	
	private final TrustedListFacade facade = TrustedListFacade.newFacade();

	@Autowired
	private ObjectMapper jsonMapper;
	@Autowired
	private Cache<String, TrustListWithHash> trustListCache;
	
	public TrustStatusListType resolveTL(String uri) {
		return resolveTL(uri, false);
	}
	
	public TrustStatusListType resolveTL(String uri, boolean validate) {
		log.debug("resolveTL.enter; got uri: {}, validate: {}", uri, validate);
		TrustListWithHash trustListHash = getTrustListWithHash(uri, validate);
		TrustStatusListType trustList = trustListHash == null ? null : trustListHash.getTrustList();
		log.debug("resolveTL.exit; returning TL: {}", trustList);
		return trustList;
	}
	
	public TLResolveResult resolveTLHash(String uri, String hash) {
		log.debug("resolveTLHash.enter; got uri: {}, hash: {}", uri, hash);

		TrustListWithHash trustListHash = getTrustListWithHash(uri, false);
		if (trustListHash == null) {
			return null;
		}

		TLResolveResult tlResult = new TLResolveResult(trustListHash.getTrustList(), null, false);
		Multihash vcHash = Multihash.fromBase58(hash); // .decode(hash); 
		Multihash.Type mType = vcHash.getType();
		if (trustListHash.getHash() == null) {
			try {
				String algo = getDigestType(mType);
				log.debug("resolveTLHash; hash algo is: {}", algo);
				MessageDigest md = MessageDigest.getInstance(algo);
				md.update(trustListHash.getContent().getBytes()); 
				Multihash tlHash = new Multihash(mType, md.digest());
				trustListHash.setHash(tlHash.toString());
				trustListHash.setContent(null); 
				tlResult.setTlHash(trustListHash.getHash());
				tlResult.setHashVerified(vcHash.equals(tlHash));
				trustListCache.put(uri, trustListHash);
			} catch (Exception ex) {
				log.warn("resolveTLHash.error verifying Hash: {}", ex.getMessage());
			}
		} else {
			tlResult.setHashVerified(hash.equals(trustListHash.getHash()));
		}
		log.debug("resolveTLHash.exit; returning: {}", tlResult);
		return tlResult;
	}
	
	private TrustListWithHash getTrustListWithHash(String uri, boolean validate) {
		TrustListWithHash trustListHash = trustListCache.getIfPresent(uri);
		if (trustListHash == null) {
			log.debug("getTrustListWithHash; no cached TL for uri: {}", uri);
			try {
				String content = resolveContent(uri);
				TrustStatusListType trustList = parseContent(content, validate);
				trustListHash = new TrustListWithHash(content, trustList, null);
				trustListCache.put(uri, trustListHash);
				log.debug("getTrustListWithHash; cached TL for uri: {}", uri);
			} catch (IOException | JAXBException | XMLStreamException | SAXException ex) {
				log.error("getTrustList.error;", ex); 
			} 
		} else {
			log.debug("getTrustListWithHash; got cached TL for uri: {}", uri);
		}
		return trustListHash;
	}
	
	private String resolveContent(String uri) throws IOException {
        URL url = new URL(uri);
	    InputStream input = url.openStream();
	    return new String(input.readAllBytes(), StandardCharsets.UTF_8);
	}
	
	private TrustStatusListType parseContent(String content, boolean validate) throws JAXBException, XMLStreamException, IOException, SAXException {
		MimeType type = getDocType(content);
		if (type == MimeTypeEnum.XML) {
			return facade.unmarshall(content, validate);
		} 
		if (type == MimeTypeEnum.JSON) {
			return jsonMapper.readValue(content, TrustStatusListType.class);
		} 
		log.info("parseContent; got content with unknown type: {}", type);
		return null;
	}

	private MimeType getDocType(String content) {
		OptionalInt opt = content.chars().filter(c -> c == '<' || c == '{').findFirst();
		if (opt.isPresent()) {
			if (opt.getAsInt() == '<') {
				return MimeTypeEnum.XML;
			} else if (opt.getAsInt() == '{') {
				return MimeTypeEnum.JSON;
			}
		}
		return MimeTypeEnum.BINARY;
	}
	
	private String getDigestType(Multihash.Type mType) throws NoSuchAlgorithmException {
		switch (mType) {
			case sha1: return "SHA-1";
			case sha2_256: return "SHA-256";
			case sha2_512: return "SHA-512";
			case sha3_256: return "SHA3-256";
		}
		throw new NoSuchAlgorithmException("Unsupported MType: " + mType);
	}
	
	@Getter
	@Setter
	@AllArgsConstructor
	@ToString
	class TLResolveResult {
		
		private TrustStatusListType trustList;
		private String tlHash;
		private boolean hashVerified;
		
	}
		
	
}
