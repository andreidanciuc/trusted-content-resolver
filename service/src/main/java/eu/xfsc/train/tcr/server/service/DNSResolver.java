package eu.xfsc.train.tcr.server.service;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.jitsi.dnssec.validator.ValidatingResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;
import org.xbill.DNS.DClass;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.PTRRecord;
import org.xbill.DNS.RRset;
import org.xbill.DNS.Rcode;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.TXTRecord;
import org.xbill.DNS.Type;
import org.xbill.DNS.URIRecord;

import eu.xfsc.train.tcr.server.exception.DnsException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("dns-resolver")
public class DNSResolver implements HealthIndicator {
	
	private static final String PREFIX = "_scheme._trust.";
	
	@Autowired
	private Resolver resolver;
	@Value("${tcr.dns.dnssec.enabled}") 
	private boolean dnssecEnabled;
	
	@Override
	public Health health() {
		Health.Builder health;
		List<String> hosts = null;
		try {
			Resolver res = resolver;
			boolean resolved = false;
			if (dnssecEnabled) {
				res = ((WrapperResolver) resolver).getWrappedResolver();
			}
			if (res instanceof SimpleResolver) {
				Pair<String, Boolean> rrr = resolveResolver((SimpleResolver) res);
				hosts = List.of(rrr.getLeft());
				resolved = rrr.getRight();
			} else { // (rs instanceof ExtendedResolver) {
				Resolver[] rss = ((ExtendedResolver) res).getResolvers();
				hosts = new ArrayList<>(rss.length);
				for (Resolver rs: rss) {
					Pair<String, Boolean> rrr = resolveResolver((SimpleResolver) rs);
					hosts.add(rrr.getLeft());
					if (rrr.getRight()) {
						resolved = rrr.getRight();
					}
				}
			}
			health = resolved ? Health.up() : Health.outOfService();
		} catch (Exception ex) {
			log.warn("health.error: ", ex);
			health = Health.down(ex);
		}
		return health.withDetail("resolverIPs", hosts).withDetail("dnsSecEnabled", dnssecEnabled).build();
	}
	
	private Pair<String, Boolean> resolveResolver(SimpleResolver res) {
		//log.debug("hostName: {}", res.getAddress().getHostName());
		return Pair.of(res.getAddress().getAddress().getHostAddress() + ":" + res.getAddress().getPort(), !res.getAddress().isUnresolved());
	}
	
	public Collection<String> resolveDomain(String domain) { 
		log.debug("resolveDomain.enter; got domain: {}", domain);
		Set<String> processed = new HashSet<>();
	    Set<String> results = resolveDomain(processed, domain); 
		log.debug("resolveDomain.exit; returning: {}", results);
		return results;
	}
	
	private Set<String> resolveDomain(Set<String> processed, String domain) { 
		domain = fixUri(domain);
		if (processed.contains(domain)) {
			return Collections.emptySet();
		}

		Set<String> uris;
		try {
			uris = resolvePtr(domain);
		} catch (IOException ex) {
			throw new DnsException(ex);
		}
	    processed.add(domain);
	    List<Exception> errors = new ArrayList<>();
		Set<String> results = new HashSet<>();
	    for (String uri: uris) {
	    	try {
	    		results.addAll(resolveUri(uri));
	    	} catch (IOException ex) {
	    		log.warn("resolveDomain; error processing URI [{}]: {}", uri, ex.getMessage());
	    		errors.add(ex);
	    	}
	        results.addAll(resolveDomain(processed, uri));
	    }
	    if (results.isEmpty() && !errors.isEmpty()) {
	    	// collect all exceptions into one?
	    	throw new DnsException(errors.get(0));
	    }
		return results;
	}
	
    /**
     * resolves DNS PTR record
     * 
     * @param ptr - the PTR record to query
     * @return the resolved Set<String> 
     */
    private Set<String> resolvePtr(String ptr) throws IOException {
        log.debug("resolvePtr.enter; got PTR: {}", ptr);
        Set<String> set = query(ptr, Type.PTR)
           	.filter(r -> r instanceof PTRRecord)
            .map(r -> ((PTRRecord) r).getTarget().toString())
            .collect(Collectors.toSet());
        log.debug("resolvePtr.exit; returning uris: {}", set);
        return set;
    }
    
    /**
     * resolves DNS URI record
     * 
     * @param ptr - the URI record to query
     * @return the resolved Set<String> 
     */
    private Set<String> resolveUri(String uri) throws IOException {
        log.debug("resolveUri.enter; got URI: {}", uri);
        Set<String> set = query(uri, Type.URI)
           	.filter(r -> r instanceof URIRecord)
            .map(r -> ((URIRecord) r).getTarget())
            .collect(Collectors.toSet());
        log.debug("resolveUri.exit; returning dids: {}", set);
        return set;
    }
	
	/**
	 * 
	 * @param uri - the domain to be queried
	 * @param type - DNS record type
	 * @return the stream of DNS records
	 * @throws IOException
	 */
    private Stream<Record> query(String uri, int type) throws IOException {
        Record query = Record.newRecord(Name.fromConstantString(uri), type, DClass.IN);
        log.debug("query; DNS query: {}", query);
        Message response = resolver.send(Message.newQuery(query));
		//log.debug("query; response: {}", response);

        int rcode = response.getRcode();
        log.debug("query; got RCode: {} ({}); AD Flag present: {}", rcode, Rcode.string(rcode), response.getHeader().getFlag(Flags.AD));
        if (rcode != Rcode.NOERROR) {
        	// the code below is just to log an additional info about error condition
            for (RRset rrSet: response.getSectionRRsets(Section.ADDITIONAL)) {
                log.debug("query; Zone: {}", rrSet.getName());
                if (rrSet.getName().equals(Name.root) && rrSet.getType() == Type.TXT && rrSet.getDClass() == ValidatingResolver.VALIDATION_REASON_QCLASS) {
                    log.info("query; Reason: {}", ((TXTRecord) rrSet.first()).getStrings().get(0));
                }
            }
            return Stream.empty();
        }
        
        List<RRset> rrSets = response.getSectionRRsets(Section.ANSWER);
        return rrSets.stream().flatMap(s -> s.rrs().stream());
    }
    
    private String fixUri(String uri) {
        if (!uri.startsWith(PREFIX)) {
            uri = PREFIX + uri;
        }
        if (!uri.endsWith(".")) {
            uri = uri + ".";
        }
        return uri;
    }

}
