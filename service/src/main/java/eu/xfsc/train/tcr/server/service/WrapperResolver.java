package eu.xfsc.train.tcr.server.service;

import org.jitsi.dnssec.validator.ValidatingResolver;
import org.xbill.DNS.Resolver;

public class WrapperResolver extends ValidatingResolver {
	
	private Resolver _resolver;

	public WrapperResolver(Resolver headResolver) {
		super(headResolver);
		this._resolver = headResolver;
	}
	
	public Resolver getWrappedResolver() {
		return _resolver;
	}

}
