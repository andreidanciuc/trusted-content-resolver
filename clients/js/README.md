# JS/TypeScript implementation for BDD tests and clients

Credits to https://www.elliotdenolf.com/blog/cucumberjs-with-typescript.

## How to Run

### Steps

- `git clone` repo, `cd` into directory
- make test # look also for all available commands in Makefile
- make install # install only once
- make install --always-make # force install
